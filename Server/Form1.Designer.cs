﻿namespace Server
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
      this.connectButton = new System.Windows.Forms.Button();
      this.labelMessage = new System.Windows.Forms.Label();
      this.labelIP = new System.Windows.Forms.Label();
      this.tbIP = new System.Windows.Forms.TextBox();
      this.labelPort = new System.Windows.Forms.Label();
      this.tbPort = new System.Windows.Forms.TextBox();
      this.SuspendLayout();
      // 
      // connectButton
      // 
      this.connectButton.ForeColor = System.Drawing.Color.Black;
      this.connectButton.Location = new System.Drawing.Point(39, 142);
      this.connectButton.Name = "connectButton";
      this.connectButton.Size = new System.Drawing.Size(75, 23);
      this.connectButton.TabIndex = 0;
      this.connectButton.Text = "Create";
      this.connectButton.UseVisualStyleBackColor = true;
      this.connectButton.Click += new System.EventHandler(this.connectButton_Click);
      // 
      // labelMessage
      // 
      this.labelMessage.AutoSize = true;
      this.labelMessage.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
      this.labelMessage.ForeColor = System.Drawing.Color.Purple;
      this.labelMessage.Location = new System.Drawing.Point(37, 21);
      this.labelMessage.Name = "labelMessage";
      this.labelMessage.Size = new System.Drawing.Size(191, 17);
      this.labelMessage.TabIndex = 5;
      this.labelMessage.Text = "Youd device is disconnected!";
      // 
      // labelIP
      // 
      this.labelIP.AutoSize = true;
      this.labelIP.ForeColor = System.Drawing.Color.Purple;
      this.labelIP.Location = new System.Drawing.Point(36, 65);
      this.labelIP.Name = "labelIP";
      this.labelIP.Size = new System.Drawing.Size(58, 13);
      this.labelIP.TabIndex = 6;
      this.labelIP.Text = "IP Address";
      // 
      // tbIP
      // 
      this.tbIP.Location = new System.Drawing.Point(125, 58);
      this.tbIP.Name = "tbIP";
      this.tbIP.ReadOnly = true;
      this.tbIP.Size = new System.Drawing.Size(103, 20);
      this.tbIP.TabIndex = 7;
      this.tbIP.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // labelPort
      // 
      this.labelPort.AutoSize = true;
      this.labelPort.ForeColor = System.Drawing.Color.Purple;
      this.labelPort.Location = new System.Drawing.Point(37, 101);
      this.labelPort.Name = "labelPort";
      this.labelPort.Size = new System.Drawing.Size(26, 13);
      this.labelPort.TabIndex = 8;
      this.labelPort.Text = "Port";
      // 
      // tbPort
      // 
      this.tbPort.Location = new System.Drawing.Point(125, 94);
      this.tbPort.MaxLength = 4;
      this.tbPort.Name = "tbPort";
      this.tbPort.Size = new System.Drawing.Size(100, 20);
      this.tbPort.TabIndex = 9;
      this.tbPort.Text = "5555";
      this.tbPort.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(258, 177);
      this.Controls.Add(this.tbPort);
      this.Controls.Add(this.labelPort);
      this.Controls.Add(this.tbIP);
      this.Controls.Add(this.labelIP);
      this.Controls.Add(this.labelMessage);
      this.Controls.Add(this.connectButton);
      this.ForeColor = System.Drawing.Color.CornflowerBlue;
      this.Name = "Form1";
      this.Text = "Android Pointer";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_Closing);
      this.Load += new System.EventHandler(this.Form1_Load);
      this.ResumeLayout(false);
      this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button connectButton;
        private System.Windows.Forms.Label labelMessage;
        private System.Windows.Forms.Label labelIP;
        private System.Windows.Forms.TextBox tbIP;
        private System.Windows.Forms.Label labelPort;
        private System.Windows.Forms.TextBox tbPort;
    }
}

