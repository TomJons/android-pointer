﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Reflection;
using Microsoft.Win32;

namespace Server
{
  public partial class Form1 : Form
  {
    public Thread listener;
    public bool keepGoing = true;
    public bool messageFlag;

    public Form1(string[] args)
    {
      InitializeComponent();
      if(args.Length > 0)
      {
        if (args[args.Length - 1] == "-v")
          messageFlag = true;
        else
          messageFlag = false;
      }
    }

    private void connectButton_Click(object sender, EventArgs e)
    {
      listener = new Thread(new ThreadStart(makeSocket));
      listener.Start();
      labelMessage.Text = "Your device is ready!";
      labelMessage.ForeColor = Color.Green;
    }


    private void makeSocket()
    {
      try
      {
        int listenPort = int.Parse(tbPort.Text);

        byte[] data = new byte[1024];
        IPEndPoint serverEP = new IPEndPoint(IPAddress.Any, listenPort);
        UdpClient newsock = new UdpClient(serverEP);

        if(messageFlag)
          MessageBox.Show("Nowy watek czekam na klienta");

        IPEndPoint client = new IPEndPoint(IPAddress.Any, 0);

        while (keepGoing)
        {
          //receive data 
          data = newsock.Receive(ref client);
          string dataTxt = Encoding.ASCII.GetString(data);   

          short action = 0;
          short firstShort = 0;
          short secandShort = 0;
          try
          {
            Array.Reverse(data);      //bigEndianToLittle or something.. xD
            action = BitConverter.ToInt16(data, 4);
            firstShort = BitConverter.ToInt16(data, 2);
            secandShort = BitConverter.ToInt16(data, 0);
            if(messageFlag)
              MessageBox.Show(action.ToString() + " "
                              + firstShort.ToString() + " "
                              + secandShort.ToString());
          }
          catch (ArgumentException)
          {
            continue;
            //do nothing 
          }

          bool cursorChanged = false;
          switch (action)
          {
            case 0:
              if (!cursorChanged)
              {
                ChangeCursor(@"ruby_in_diamonds.cur");
                cursorChanged = true;
              }
              
              Cursor.Position = new Point(Cursor.Position.X + firstShort, Cursor.Position.Y + secandShort);
              break;

            //slides and presentations
            case 1:
              if (firstShort == 0)
                SendKeys.SendWait("{LEFT}");

              else
                SendKeys.SendWait("{RIGHT}");

              break;
          }
        }

        //back to the standard cursor
        ChangeCursor(""); 
        //close socket
        newsock.Close();
      }
      catch (Exception ex)
      {
        if (messageFlag)
          MessageBox.Show(ex.ToString());
      }
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      tbIP.Text = Dns.GetHostEntry(Dns.GetHostName()).AddressList.FirstOrDefault(ip => ip.AddressFamily == AddressFamily.InterNetwork).ToString();
    }

    private void Form1_Closing(object sender, FormClosingEventArgs e)
    {
      keepGoing = false;
      listener.Join();

      if (messageFlag) 
        MessageBox.Show("Koncze dzialanie");
    }

    private void ChangeCursor(string curFile)
    {
      Registry.SetValue(@"HKEY_CURRENT_USER\Control Panel\Cursors\", "Arrow", curFile);
      SystemParametersInfo(SPI_SETCURSORS, 0, 0, SPIF_UPDATEINIFILE | SPIF_SENDCHANGE);
    }

    const int SPI_SETCURSORS = 0x0057;
    const int SPIF_UPDATEINIFILE = 0x01;
    const int SPIF_SENDCHANGE = 0x02;

    [DllImport("user32.dll", EntryPoint = "SystemParametersInfo")]
    private static extern bool SystemParametersInfo(uint uiAction, uint uiParam, uint pvParam, uint fWinIni);
  }
}
