package com.example.androidpointer.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.net.InetAddress;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        // Disable autorotating.
        Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0);

        Button connectBtn = (Button) findViewById(R.id.button_connect);
        connectBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                String ip = null;
                Integer port = null;
                Boolean isConnect = false;

                Intent nextAction = new Intent(view.getContext(), PointerActivity.class);
                //getting ip and port from text fields
                try {
                    ip = ((EditText) findViewById(R.id.editTextIP)).getText().toString();
                    port = Integer.valueOf(
                            ((EditText) findViewById(R.id.editTextPort)).getText().toString());
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }

                //creating socket
                try {
                    SingletonSocket.getInstance();
                    SingletonSocket.socket.connect(InetAddress.getByName(ip), port);
                    isConnect = true;
                } catch (IOException e) {
                    isConnect = false;
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    isConnect = false;
                    e.printStackTrace();
                }

                //if connected kill login activity and start pointer activity
                if (isConnect) {
                    startActivity(nextAction);
                    finish();
                }
            }

        });
    }
    // Application lifecycle: lock autorotating if application is active.
    @Override
    protected void onPause() {
        super.onPause();
        // Unlock autorotating.
        Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 1);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Lock autorotating.
        Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0);
    }

}
