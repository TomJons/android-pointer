package com.example.androidpointer.app;

import java.net.DatagramSocket;
import java.net.SocketException;

/**
 * Created by Jojo on 2014-05-20.
 */
public final class SingletonSocket {

    // należy zwrócić uwagę na użycie słowa kluczowego volatile
    private static volatile SingletonSocket instance = null;
    public static DatagramSocket socket;

    public static SingletonSocket getInstance() {
        if (instance == null) {
            synchronized (SingletonSocket.class) {
                if (instance == null) {
                    instance = new SingletonSocket();
                }
            }
        }
        return instance;
    }

    // żeby uniknąć automatycznego tworzenia domyślnego, publicznego, bezargumentowego konstruktora
    private SingletonSocket() {
        try {
            this.socket=new DatagramSocket();
        } catch (SocketException e) {
            e.printStackTrace();
        }
    }



}
