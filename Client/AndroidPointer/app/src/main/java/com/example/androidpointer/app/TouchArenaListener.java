package com.example.androidpointer.app;

import android.view.MotionEvent;
import android.view.View;

import java.io.IOException;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;

public class TouchArenaListener implements View.OnTouchListener {
    // Variables for storing previous cursor position for calculating movement.
    private short prevX = 0;
    private short prevY = 0;
    // Variables for reading current cursor position.
    private short x = 1;
    private short y = 1;
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        short tmpX, tmpY;
        byte [] packetData;
        DatagramPacket packet;

        switch (event.getAction()) {

            case MotionEvent.ACTION_DOWN:
                prevX = (short) new Float(event.getX()).intValue();
                prevY = (short) new Float(event.getY()).intValue();
                break;
            case MotionEvent.ACTION_MOVE:
                //getting finger position
                x = (short) new Float(event.getX()).intValue();
                y = (short) new Float(event.getY()).intValue();
                tmpX = x;
                tmpY = y;

                //subtract current position from starting position
                x -= prevX;
                y -= prevY;

                //save current coordinates as previous
                prevX = tmpX;
                prevY = tmpY;

                //converting position data to buffer format
                packetData = ByteBuffer.allocate(6).putShort((short) 0).putShort(x).putShort(y).array();

                //create packet
                packet = new DatagramPacket(packetData, packetData.length);

                //sending packet in other thread
                try {
                    SingletonSocket.socket.send(packet);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                break;

        }

        return true;
    }
}