package com.example.androidpointer.app;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.provider.Settings;
import android.view.GestureDetector;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.io.IOException;
import java.net.DatagramPacket;
import java.nio.ByteBuffer;


public class PointerActivity extends MainActivity implements SensorEventListener, OnCheckedChangeListener {

    // ToggleButton for enabling/disabling actions.
    private ToggleButton actionsSwitch;
    // Message for user.
    TextView actionMsg;
    // Sensors classes.
    private SensorManager sm;
    private Sensor accelerometer;
    // Touchpad for moving cursor.
    View touchPad;
    TouchArenaListener touchPadListener;
    // Coordinates
    private float lastX;
    // Precision
    private final float PRECISION = (float) 5.0;
    // Slide action recognizing
    private enum Slide { PREVIOUS, NEXT};
    // Time between slide change requests in miliseconds.
    // 1.000.000 ms = 1 second.
    private final int SLIDE_REQUESTS_IN_MS = 500000;

    private GestureDetector gestureScanner;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.touchpad);
        // Get actionMsg field
        actionMsg = (TextView)findViewById(R.id.actionMessage);
        // Getting device sensor into our object.
        sm = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sm.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        //Initizialize touchPads.
        touchPad = findViewById(R.id.TouchArena);
        touchPadListener = new TouchArenaListener();
        // Assign listener to actionsSwitch
        actionsSwitch = (ToggleButton) findViewById(R.id.actions_switch);
        actionsSwitch.setOnCheckedChangeListener(this);
        actionsSwitch.setChecked(true);
        //Create listeners for slide buttons
        findViewById(R.id.NextSlideBtn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                sendSlidePacket(Slide.NEXT);
            }
        });
        findViewById(R.id.PrevSlideBtn).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                sendSlidePacket(Slide.PREVIOUS);
            }
        });
        // Create listener for exit button
        findViewById(R.id.exit).setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });
    }

    // Activity's LifeCycle:
    protected void onPause() {
        super.onPause();
        actionsSwitch.setChecked(false);
    }

    protected void onResume() {
        super.onResume();
    }

    // Listener for actionsSwitch
    @Override
    public void onCheckedChanged(CompoundButton btn, boolean isChecked) {
        TextView helpText = (TextView)findViewById(R.id.help_text);
        if(isChecked) {
            //Gesture enable, slide buttons invisible
            findViewById(R.id.SlideButtonSection).setVisibility(View.GONE);
            // Hide exit button
            findViewById(R.id.exit).setVisibility(View.GONE);
            // App resumed, go back to listening to data.
            sm.registerListener(this, accelerometer, SLIDE_REQUESTS_IN_MS );
            touchPad.setOnTouchListener(touchPadListener);
            // Also lock iritating autorotation.
            Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 0);
            // Set help text.
            helpText.setText("Lean phone right for next slide.\n" +
                    "Lean phone left for previous slide.\n" +
                    "Use space under switch button below for moving the cursor.\n" +
                    "You can disable gesture actions and enable button-driven \n" +
                    "actions by setting switch button to OFF. \n" +
                    "To exit application disable gesture actions first.");
            // Set action message.
            actionMsg.setText("Connection ready for sending events.");

        } else {
            // Gesture disable, slide buttons visible
            findViewById(R.id.SlideButtonSection).setVisibility(View.VISIBLE);
            // Show exit button
            findViewById(R.id.exit).setVisibility(View.VISIBLE);
            // App is paused, no need to listen to data.
            sm.unregisterListener(this);
            touchPad.setOnTouchListener(null);
            // Also unlock autorotation.
            Settings.System.putInt(getContentResolver(), Settings.System.ACCELEROMETER_ROTATION, 1);
            // Set help text.
            helpText.setText("Gesture Actions are currently disabled.\n" +
                    "You can enable them by setting switch button to ON.");
            actionMsg.setText("");

        }
    }

    @Override

    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Idk what happens here. Whatever.
    }

    /**
     * Creates and sends to SingletonSocket packet with information about changing slide.
     * @param slide determining if slide is switched forwards or backwards.
     */
    private void sendSlidePacket(Slide slide) {
        // First byte - type of request:
        //  1 - slide change
        // Second byte - direction
        //  0 - move slide backwards
        //  1 - move slide forwards
        // Third byte - random data that will be ignored.
        byte[] packetData = ByteBuffer.allocate(6).putShort((short) 1).putShort((short) slide.ordinal()).putShort((short) 0).array();
        try {
            DatagramPacket packet = new DatagramPacket(packetData, packetData.length);
            SingletonSocket.socket.send(packet);
        } catch (java.net.SocketException se) {
            se.printStackTrace();
        } catch (IOException io) {
            io.printStackTrace();
        }
    }

    // Listener for accelerometer
    @Override
    public void onSensorChanged(SensorEvent event) {
        TextView actionMsg = (TextView)findViewById(R.id.actionMessage);
        float x = event.values[0];
        Slide slide;
        // Check if movement is not just accident.
        float absMovementX = Math.abs(lastX - x);
        float movementX = lastX - x;
        // If it is an accident, then 0 the movement.
        if (absMovementX < PRECISION) movementX = (float) 0.0;
        // Assign new coordinates.
        lastX = (float) 0.0;
        // Decide if we switch slide to next, or to previous.
        if(movementX < 0) {
            slide = Slide.PREVIOUS;
            sendSlidePacket(slide);
            actionMsg.setText("Sent previous slide request.");
        } else if(movementX > 0) {
            slide = Slide.NEXT;
            sendSlidePacket(slide);
            actionMsg.setText("Sent next slide request.");
        }

    }


}