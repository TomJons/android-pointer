﻿namespace ServerTest
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sendButton = new System.Windows.Forms.Button();
            this.ipLbl = new System.Windows.Forms.Label();
            this.portLbl = new System.Windows.Forms.Label();
            this.firstBLbl = new System.Windows.Forms.Label();
            this.ipTb = new System.Windows.Forms.TextBox();
            this.portTb = new System.Windows.Forms.TextBox();
            this.dataSendTb1 = new System.Windows.Forms.TextBox();
            this.dataSendTb2 = new System.Windows.Forms.TextBox();
            this.dataSendTb3 = new System.Windows.Forms.TextBox();
            this.secBLbl = new System.Windows.Forms.Label();
            this.thirdBLbl = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // sendButton
            // 
            this.sendButton.Location = new System.Drawing.Point(85, 216);
            this.sendButton.Name = "sendButton";
            this.sendButton.Size = new System.Drawing.Size(75, 23);
            this.sendButton.TabIndex = 0;
            this.sendButton.Text = "Send";
            this.sendButton.UseVisualStyleBackColor = true;
            this.sendButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // ipLbl
            // 
            this.ipLbl.AutoSize = true;
            this.ipLbl.Location = new System.Drawing.Point(24, 31);
            this.ipLbl.Name = "ipLbl";
            this.ipLbl.Size = new System.Drawing.Size(15, 13);
            this.ipLbl.TabIndex = 1;
            this.ipLbl.Text = "ip";
            // 
            // portLbl
            // 
            this.portLbl.AutoSize = true;
            this.portLbl.Location = new System.Drawing.Point(24, 62);
            this.portLbl.Name = "portLbl";
            this.portLbl.Size = new System.Drawing.Size(25, 13);
            this.portLbl.TabIndex = 2;
            this.portLbl.Text = "port";
            // 
            // firstBLbl
            // 
            this.firstBLbl.AutoSize = true;
            this.firstBLbl.Location = new System.Drawing.Point(24, 94);
            this.firstBLbl.Name = "firstBLbl";
            this.firstBLbl.Size = new System.Drawing.Size(23, 13);
            this.firstBLbl.TabIndex = 3;
            this.firstBLbl.Text = "first";
            // 
            // ipTb
            // 
            this.ipTb.Location = new System.Drawing.Point(63, 28);
            this.ipTb.Name = "ipTb";
            this.ipTb.Size = new System.Drawing.Size(154, 20);
            this.ipTb.TabIndex = 4;
            this.ipTb.Text = "192.168.230.1";
            // 
            // portTb
            // 
            this.portTb.Location = new System.Drawing.Point(63, 59);
            this.portTb.Name = "portTb";
            this.portTb.Size = new System.Drawing.Size(154, 20);
            this.portTb.TabIndex = 5;
            this.portTb.Text = "5555";
            // 
            // dataSendTb1
            // 
            this.dataSendTb1.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.dataSendTb1.Location = new System.Drawing.Point(63, 91);
            this.dataSendTb1.Name = "dataSendTb1";
            this.dataSendTb1.Size = new System.Drawing.Size(154, 20);
            this.dataSendTb1.TabIndex = 6;
            this.dataSendTb1.Text = "Enter data to send..";
            this.dataSendTb1.Enter += new System.EventHandler(this.firstClickToAddData);
            // 
            // dataSendTb2
            // 
            this.dataSendTb2.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.dataSendTb2.Location = new System.Drawing.Point(63, 126);
            this.dataSendTb2.Name = "dataSendTb2";
            this.dataSendTb2.Size = new System.Drawing.Size(154, 20);
            this.dataSendTb2.TabIndex = 7;
            this.dataSendTb2.Text = "Enter data to send..";
            this.dataSendTb2.Enter += new System.EventHandler(this.firstClickToAddData);
            // 
            // dataSendTb3
            // 
            this.dataSendTb3.ForeColor = System.Drawing.SystemColors.WindowFrame;
            this.dataSendTb3.Location = new System.Drawing.Point(63, 163);
            this.dataSendTb3.Name = "dataSendTb3";
            this.dataSendTb3.Size = new System.Drawing.Size(154, 20);
            this.dataSendTb3.TabIndex = 8;
            this.dataSendTb3.Text = "Enter data to send..";
            this.dataSendTb3.Enter += new System.EventHandler(this.firstClickToAddData);
            // 
            // secBLbl
            // 
            this.secBLbl.AutoSize = true;
            this.secBLbl.Location = new System.Drawing.Point(24, 129);
            this.secBLbl.Name = "secBLbl";
            this.secBLbl.Size = new System.Drawing.Size(24, 13);
            this.secBLbl.TabIndex = 9;
            this.secBLbl.Text = "sec";
            // 
            // thirdBLbl
            // 
            this.thirdBLbl.AutoSize = true;
            this.thirdBLbl.Location = new System.Drawing.Point(24, 166);
            this.thirdBLbl.Name = "thirdBLbl";
            this.thirdBLbl.Size = new System.Drawing.Size(27, 13);
            this.thirdBLbl.TabIndex = 10;
            this.thirdBLbl.Text = "third";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.thirdBLbl);
            this.Controls.Add(this.secBLbl);
            this.Controls.Add(this.dataSendTb3);
            this.Controls.Add(this.dataSendTb2);
            this.Controls.Add(this.dataSendTb1);
            this.Controls.Add(this.portTb);
            this.Controls.Add(this.ipTb);
            this.Controls.Add(this.firstBLbl);
            this.Controls.Add(this.portLbl);
            this.Controls.Add(this.ipLbl);
            this.Controls.Add(this.sendButton);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button sendButton;
        private System.Windows.Forms.Label ipLbl;
        private System.Windows.Forms.Label portLbl;
        private System.Windows.Forms.Label firstBLbl;
        private System.Windows.Forms.TextBox ipTb;
        private System.Windows.Forms.TextBox portTb;
        private System.Windows.Forms.TextBox dataSendTb1;
        private System.Windows.Forms.TextBox dataSendTb2;
        private System.Windows.Forms.TextBox dataSendTb3;
        private System.Windows.Forms.Label secBLbl;
        private System.Windows.Forms.Label thirdBLbl;
    }
}

