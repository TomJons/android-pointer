﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ServerTest
{
    public partial class Form1 : Form
    {
        private bool firstClickFirst;
        private bool firstClickSec;
        private bool firstClickThird;

        public Form1()
        {
            InitializeComponent();
            
            //
            firstClickFirst = true;
            firstClickSec = true;
            firstClickThird = true;
        }
        
        private void button1_Click(object sender, EventArgs e)
        {
            Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
            IPEndPoint serverEP = new IPEndPoint(IPAddress.Parse(ipTb.Text), Int32.Parse(portTb.Text));

            byte[] short1 = BitConverter.GetBytes(Int16.Parse(dataSendTb1.Text));
            byte[] short2 = BitConverter.GetBytes(Int16.Parse(dataSendTb2.Text));
            byte[] short3 = BitConverter.GetBytes(Int16.Parse(dataSendTb3.Text));

            byte[] buffer = new byte[6];
            for(int i = 0; i < 2; i++)
            {
                buffer[i] = short1[i];
                buffer[2 + i] = short2[i];
                buffer[3 + i] = short3[i];
            }

            client.SendTo(buffer, buffer.Length, SocketFlags.None, serverEP);
            client.Close();

        }


        //method remove text from textbox at first click on it
        private void firstClickToAddData(object sender, EventArgs e)
        {
            TextBox tb = sender as TextBox;
            bool first;
            int val = 0;

            if (tb.Name == "dataSendTb1")
            {
                first = firstClickFirst;
                val = 1;
            }
                
            else if (tb.Name == "dataSendTb2")
            {
                first = firstClickSec;
                val = 2;
           }
                
            else if (tb.Name == "dataSendTb3")
            {
                first = firstClickThird;
                val = 3;
            }
            else
                first = false;

            if (first) 
            {
                tb.Text = "";
                tb.ForeColor = Color.Black;
                first = false;
            
                switch(val)
                {
                    case 1:
                        firstClickFirst = first;
                        break;

                    case 2:
                        firstClickSec = first;
                        break;

                    case 3:
                        firstClickThird = first;
                        break;

                    default:
                        break;
                }
            }
        }   
    }
}
